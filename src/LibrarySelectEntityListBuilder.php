<?php

namespace Drupal\library_select;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Library Select entities.
 */
class LibrarySelectEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Library id');
    $header['label'] = $this->t('Label');
    $header['description'] = $this->t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = 'library_select/' . $entity->id();
    $row['label'] = $entity->label();
    $row['description'] = $entity->description ?? '';
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}
