<?php

namespace Drupal\library_select\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\options\Plugin\Field\FieldType\ListItemBase;

/**
 * Plugin implementation of the 'library_select' field type.
 *
 * @FieldType(
 *   id = "library_select_field",
 *   label = @Translation("Library Select"),
 *   description = @Translation("Library Select Field Type"),
 *   default_widget = "library_select_widget",
 *   default_formatter = "library_select_formatter"
 * )
 */
class LibrarySelectField extends ListItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $storageSettings = parent::defaultStorageSettings();
    $storageSettings['allowed_values_function'] = 'library_select_options_callback';

    return $storageSettings;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = parent::defaultFieldSettings();

    $settings['library_select'] = [];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $configs = $this->getSettings()['library_select'] ?? [];
    $form['library_select'] = [
      '#type'  => 'details',
      '#title' => $this->t('Library Select'),
      '#open'  => TRUE,
    ];

    $form['library_select']['library_select_limit'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Limit list to selected items'),
      '#required'      => FALSE,
      '#return_value'  => 1,
      '#default_value' => $configs['library_select_limit'] ?? FALSE,
    ];

    $form['library_select']['library_select_limit_values'] = [
      '#type'          => 'checkboxes',
      '#required'      => FALSE,
      '#title'         => $this->t('Limit items'),
      '#default_value' => $configs['library_select_limit_values'] ?? [],
      '#options'       => library_select_options_callback(),
      '#states'        => [
        'visible' => [
          ':input[name="settings[library_select][library_select_limit]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Library'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type'   => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function allowedValuesDescription() {
    $description = '<p>' . t('The possible values this field can contain. Enter one value per line, in the format key|label.');
    $description .= '<br/>' . t('The key is the stored value. The label will be used in displayed values and edit forms.');
    $description .= '<br/>' . t('The label is optional: if a line contains a single string, it will be used as key and label.');
    $description .= '</p>';
    $description .= '<p>' . t('Allowed HTML tags in labels: @tags', ['@tags' => FieldFilteredMarkup::displayAllowedTags()]) . '</p>';
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  protected static function validateAllowedValue($option) {
    if (strlen($option) > 255) {
      return t('Allowed values list: each key must be a string at most 255 characters long.');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected static function castAllowedValue($value) {
    return (string) $value;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->value) && (string) $this->value !== '0';
  }

}
