<?php

namespace Drupal\library_select\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'library_select_widget' widget.
 *
 * @FieldWidget(
 *   id = "library_select_widget",
 *   label = @Translation("Library Select"),
 *   field_types = {
 *     "library_select_field"
 *   },
 *   multiple_values = TRUE
 * )
 */
class LibrarySelectWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $library_select = $items->getSetting('library_select') ?? [];
    if (!empty($library_select['library_select_limit'])) {
      $all_options = library_select_options_callback();
      $options = [];
      if (isset($element['#options']['_none'])) {
        $options['_none'] = $element['#options']['_none'];
      }
      foreach ($library_select['library_select_limit_values'] as $item) {
        if (isset($all_options[$item])) {
          $options[$item] = $all_options[$item];
        }
      }
      $element['#options'] = $options;
    }
    return $element;
  }

}
