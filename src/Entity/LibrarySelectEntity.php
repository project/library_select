<?php

namespace Drupal\library_select\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\library_select\LibraryFileStorage;

/**
 * Defines the Library Select entity.
 *
 * @ConfigEntityType(
 *   id = "library_select_entity",
 *   label = @Translation("Library Select"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\library_select\LibrarySelectEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\library_select\Form\LibrarySelectEntityForm",
 *       "edit" = "Drupal\library_select\Form\LibrarySelectEntityForm",
 *       "delete" = "Drupal\library_select\Form\LibrarySelectEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\library_select\LibrarySelectEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "library_select_entity",
 *   admin_permission = "administer library select",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *   "id",
 *   "label",
 *   "uuid",
 *   "css_code",
 *   "js_code",
 *   "css_files",
 *   "js_files",
 *   "dependencies_libs",
 *   "description"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/development/library_select_entity/{library_select_entity}",
 *     "add-form" = "/admin/config/development/library_select_entity/add",
 *     "edit-form" = "/admin/config/development/library_select_entity/{library_select_entity}/edit",
 *     "delete-form" = "/admin/config/development/library_select_entity/{library_select_entity}/delete",
 *     "collection" = "/admin/config/development/library_select_entity"
 *   }
 * )
 */
class LibrarySelectEntity extends ConfigEntityBase implements LibrarySelectEntityInterface {

  /**
   * The Library Select ID.
   */
  protected string $id;

  /**
   * The Library Select label.
   */
  protected string $label;

  /**
   * The Library Select description.
   */
  public string $description;

  /**
   * The custom code of the css.
   */
  public string $css_code;

  /**
   * The custom code of the js.
   */
  public string $js_code;

  /**
   * The list of the css.
   */
  public string $css_files;

  /**
   * The list of the js.
   */
  public string $js_files;

  /**
   * Allow pre process css, js.
   */
  public bool $preprocess = FALSE;

  /**
   * Css extension.
   */
  public string $cssExtension = 'css';

  /**
   * JS extension.
   */
  public string $jsExtension = 'js';

  /**
   * Library dependencies.
   */
  public string $dependencies_libs = '';

  /**
   * Get css files.
   *
   * @return string
   *   The list of the css files.
   */
  public function getCssFiles(): string {
    return $this->css_files;
  }

  /**
   * Get css files.
   *
   * @return array
   *   The list of the css files.
   */
  public function getCssFilesArray(): array {
    return explode("\r\n", $this->getCssFiles());
  }

  /**
   * Get js files.
   *
   * @return array
   *   The list of the css files.
   */
  public function getJsFilesArray(): array {
    return explode("\r\n", $this->getJsFiles());
  }

  /**
   * Get js files.
   *
   * @return string
   *   The list of the js files.
   */
  public function getJsFiles(): string {
    return $this->js_files;
  }

  /**
   * Get css code.
   *
   * @return string
   *   The code.
   */
  public function getCssCode(): string {
    return $this->css_code;
  }

  /**
   * Get js code.
   *
   * @return string
   *   The code.
   */
  public function getJsCode(): string {
    return $this->js_code;
  }

  /**
   * Get js code.
   *
   * @return array
   *   The code.
   */
  public function getDependenciesArray(): array {
    if (mb_strlen(trim($this->dependencies_libs)) > 0) {
      $dependencies = [];
      $libs = explode("\r\n", trim($this->dependencies_libs));
      /** @var \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery */
      $library_discovery = \Drupal::service('library.discovery');
      foreach ($libs as $lib) {
        $info = explode('/', $lib, 2);
        if (count($info) == 2 && $library_discovery->getLibraryByName($info[0], $info[1])) {
          $dependencies[] = $lib;
        }
      }
      return $dependencies;
    }

    return [];
  }

  /**
   * Get custom code.
   *
   * @param string $type
   *   The type.
   *
   * @return string
   *   The code.
   */
  public function getCode(string $type): string {
    $token = \Drupal::token();
    if ($type === 'css') {
      return $token->replace($this->getCssCode());
    }

    if ($type === 'js') {
      return $token->replace($this->getJsCode());
    }
    return '';
  }

  /**
   * Gets the library array used in library_info_build.
   *
   * @return array
   *   Library info array for this asset.
   */
  public function libraryInfo(): array {
    $library_info = [];
    $dependencies = $this->getDependenciesArray();
    if (!empty($dependencies)) {
      $library_info['dependencies'] = $dependencies;
    }

    foreach ($this->getJsFilesArray() as $path) {
      $path = trim($path);
      if (!empty($path)) {
        $library_info['js'][$path] = ['preprocess' => $this->preprocess];
      }
    }

    if (!empty($this->getJsCode())) {
      $path = $this->filePathRelativeToDrupalRoot($this->jsExtension);
      $library_info['js'][$path] = ['preprocess' => $this->preprocess];
    }

    foreach ($this->getCssFilesArray() as $path) {
      if (!empty($path)) {
        $library_info['css']['theme'][$path] = [
          'weight' => 0,
          'media' => 'all',
          'preprocess' => !str_contains($path, '//'),
        ];
      }
    }

    if (!empty($this->getCssCode())) {
      $path = $this->filePathRelativeToDrupalRoot($this->cssExtension);
      $library_info['css']['theme'][$path] = [
        'weight' => 0,
        'media' => 'all',
        'preprocess' => TRUE,
      ];
    }

    return $library_info;
  }

  /**
   * Get internal file uri.
   *
   * @param string $type
   *   The type css or js.
   *
   * @return string
   *   The file uri.
   */
  public function internalFileUri(string $type): string {
    $storage = new LibraryFileStorage($this);
    return $storage->createFile($type);
  }

  /**
   * Get file path relative to drupal root to use in library info.
   *
   * @param string $type
   *   The file type, css or js.
   *
   * @return string
   *   File path relative to drupal root, with leading slash.
   */
  protected function filePathRelativeToDrupalRoot(string $type): string {
    // @todo See if we can simplify this via file_url_transform_relative().
    $path = parse_url(\Drupal::service('file_url_generator')
      ->generateAbsoluteString($this->internalFileUri($type)), PHP_URL_PATH);
    $path = str_replace(base_path(), '/', $path);
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $original_id = $this->getOriginalId();
    if ($original_id) {
      $original = $storage->loadUnchanged($original_id);
      // This happens to fail on config import.
      if ($original instanceof LibrarySelectEntityInterface) {
        $asset_file_storage = new LibraryFileStorage($original);
        $asset_file_storage->deleteFiles();
      }
    }
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    foreach ($entities as $entity) {
      /** @var \Drupal\library_select\Entity\LibrarySelectEntity $entity */
      $original_id = $entity->getOriginalId();
      if ($original_id) {
        $original = $storage->loadUnchanged($original_id);
        // This happens to fail on config import.
        if ($original instanceof LibrarySelectEntityInterface) {
          $asset_file_storage = new LibraryFileStorage($original);
          $asset_file_storage->deleteFiles();
        }
      }
    }
    parent::preDelete($storage, $entities);
  }

}
